import java.util.InputMismatchException;
import java.util.Objects;
import java.util.Scanner;

public class RunGame extends Game{
    public static void main(String[] args) {
        boolean yes = true;
        Game g1 = new Game();
        while(yes){
            g1.startGame();
            String reply = "";
            System.out.println("Would you like to play again? (y or n) ");
            try {
                Scanner gs = new Scanner(System.in);
                reply = gs.nextLine();
                if(!Objects.equals(reply, "n") && !Objects.equals(reply,"y")) throw new InputMismatchException("That isn't what we asked...");
            }
            catch (Exception e){
                System.out.println(" Your input didnt match our requirements...");
                yes = false;
            }
            if(reply.equals("n")){
                break;
            }
        }
    }


}
