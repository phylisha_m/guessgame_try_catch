public class Grader{

    public boolean gradeGuess(int userGuess, int num){
        boolean ans = false;

        if(userGuess == num)
            ans = true;
        else if(userGuess < num){
            System.out.println("Your Guess is too low");
            ans = false;
        }
        if(userGuess > num){
            System.out.println("Your Guess is too high");
            ans = false;
        }

        return ans;
    }
}
