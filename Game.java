public class Game extends Player{


    public void startGame(){

        Player p = new Player();
        Grader g = new Grader();
        int count = 0;
        String name = p.retrieveName();
        int num = (int)((Math.random() * (20 -1)) + 1);
        while(count <= 5){

            int userGuess = p.makeGuess() ;

            boolean ans = g.gradeGuess(userGuess, num);

            if(ans){
                break;
            }
            count++;
        }

        if(count <= 5){
            System.out.println("Good job "+ name + "! You guessed my number in " + (count + 1) + " guesses!");
        }
    }

}

